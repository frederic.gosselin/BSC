#!/bin/bash
#SBATCH --job-name=exoBSC1
#SBATCH -p q-128Go
#SBATCH -o /scratch/frederic.gosselin/%j.out
#SBATCH -e /scratch/frederic.gosselin/%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=frederic.gosselin@irstea.fr


module load gcc/5.2.0

cd /nfs/home/frederic.gosselin/save/BSC


/cm/shared/apps/R/R-3.2.4/bin/R CMD BATCH --no-save exoBSC1.txt

